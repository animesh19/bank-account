import React from 'react';
import CellListItem from '../cell-list-item';
import TableHeader from '../table-header';
import './styles.css';

class Table extends React.Component {
  render() {
    const rows = [];

    this.props.transactions.forEach(transaction => {
      rows.push(
        <CellListItem transaction={transaction} key={transaction.id} />
      );
    });

    return (
      <table className="datatable">
        <tbody>
          <TableHeader />
          {rows}
        </tbody>
      </table>
    );
  }
}

export default Table;
