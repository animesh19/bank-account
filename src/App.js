import React, { useState, useEffect } from 'react';
import './App.css';
import fetchTransactions from './server';
import setTransactions from './set-transactions';
import FilterableTransactionTable from './filterableTransactionTable';

const App = () => {
  const [transactions, getTransactions] = useState([]);

  useEffect(() => {
    fetchTransactions(getTransactions);
  }, []);

  const computedTransactions = transactions && setTransactions(transactions);

  return (
    <div className="container">
      {computedTransactions.length ? <FilterableTransactionTable transactions = {computedTransactions} /> : <h2>Loading...</h2>}
    </div>
  );
};

export default App;
