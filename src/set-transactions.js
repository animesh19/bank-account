const setTransactions = transactions => {
 let i = 1;

 // Reverse the transactions so that the latest transaction is shown at the top

  return transactions.map(transaction => {
    return {
      acNumber: transaction["Account No"],
      balance: transaction["Balance AMT"],
      date: transaction["Date"],
      deposit: transaction["Deposit AMT"],
      description: transaction["Transaction Details"],
      id: i++,
      valueDate: transaction["Value Date"],
      withdrawal: transaction["Withdrawal AMT"]
    };
  }).reverse();
};

export default setTransactions;
