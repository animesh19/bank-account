import React from 'react';

const CellListItem = ({ transaction }) => {
    const { balance, date, deposit, description, id, valueDate, withdrawal } = transaction;
    
    return (
        <tr key={id}>
          <td>{date}</td>
          <td>{description}</td>
          <td>{valueDate}</td>
          <td>{deposit}</td>
          <td>{withdrawal}</td>
          <td>{balance}</td>
        </tr>
    );
}

export default CellListItem;


