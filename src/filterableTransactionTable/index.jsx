import React from 'react';
import Pagination from '../pagination';
import SearchBar from '../search-bar';
import Table from '../table';
import TopHeader from '../top-header';


class FilterableTransactionTable extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      currentPage: "1",
      transactionType: "all"
    };

    this.handleDropdownChange = this.handleDropdownChange.bind(this);
    this.paginate = this.paginate.bind(this);
  }

  handleDropdownChange = event => {
   // Set current page to 1 so that changing transaction type starts from page 1
    this.setState({ currentPage: 1, transactionType: event });
  };

  paginate = pageNumber => {
    this.setState({ currentPage: pageNumber });
  };

  // Get filtered transactions based on the transaction type selected by the user
  getFilteredTransactions = transactions => {
   let currentTransactions = transactions;
   if (this.state.transactionType === 'deposits') {
    currentTransactions = transactions.filter(transaction => 
     transaction.withdrawal === ""
    )
   } else if (this.state.transactionType === 'withdrawals') {
    currentTransactions = transactions.filter(transaction => 
     transaction.deposit === ""
    )
   }

   return currentTransactions;
  }

  render() {
    const { transactions } = this.props;

    // Account number and Balance are always going to be constant
    const transaction = transactions && transactions[0];
    const accountNumber = transaction && transaction.acNumber;

    const lastTransaction = transactions && transactions[0];
    const balance = lastTransaction && lastTransaction.balance;

    // Get current transactions
   const indexOfLastTransaction = this.state.currentPage * 10;
   const indexOfFirstTransaction = indexOfLastTransaction - 10;
   const currentTransactions = this.getFilteredTransactions(transactions).slice(
     indexOfFirstTransaction,
     indexOfLastTransaction
   );

    return (
      <div className="container">
        <SearchBar onDropdownChange={this.handleDropdownChange} />
        <TopHeader acNumber={accountNumber} balance={balance} />
        <Table transactions={currentTransactions} />
        <Pagination
          paginate={this.paginate}
          totalTransactions={this.getFilteredTransactions(transactions).length}
        />
      </div>
    );
  }
}

export default FilterableTransactionTable;
