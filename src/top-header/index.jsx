import React from 'react';
import { number, string } from 'prop-types';
import './styles.css';

class TopHeader extends React.Component {
 render() {
   return (
    <div>
     <p>
      Account Number: <b>{ this.props.acNumber }</b>
     </p>
     <p>
      Balance: <b>{ this.props.balance }</b>
     </p>
    </div>
   )
 }

 static propTypes ={
   acNumber: number.isRequired,
   balance: string.isRequired
 }
}

export default TopHeader;