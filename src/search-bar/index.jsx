import React from 'react';

class SearchBar extends React.Component {
  constructor(props) {
    super(props);

    this.handleDropdownChange = this.handleDropdownChange.bind(this);
  }

  handleDropdownChange = event => {
    this.props.onDropdownChange(event.target.value);
  };

  render() {
    return (
      <form>
        <p>
          Transaction Type:{" "}
          {
            <select
              value={this.props.dropdownValue}
              onChange={this.handleDropdownChange}
            >
              <option value="all">All</option>
              <option value="deposits">Deposits</option>
              <option value="withdrawals">Withdrawals</option>
            </select>
          }
        </p>
      </form>
    );
  }
}

export default SearchBar;
