import axios from 'axios';

const URL = 'https://cors-anywhere.herokuapp.com/http://starlord.hackerearth.com/bankAccount';

const fetchTransactions = async(setTransactions) => {
 const response = await axios.get(URL);
 setTransactions(response.data);
}

export default fetchTransactions;