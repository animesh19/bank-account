import React from 'react';

const TRANSACTIONS_PER_PAGE = 10;

const Pagination = ({ paginate, totalTransactions }) => {
  const pageNumbers = [];

  for (let i = 1; i <= Math.ceil(totalTransactions / TRANSACTIONS_PER_PAGE); i++) {
    pageNumbers.push(i);
  }

  return (
   <div>
    <nav>
      <ul className="pagination">
        {pageNumbers.map(number => (
          <li key={number} className="page-item">
            <a
              onClick={() => paginate(number)}
              href="!#"
              className="page-link"
            >
              {number}
            </a>
          </li>
        ))}
      </ul>
    </nav>
    </div>
  );
};

export default Pagination;
